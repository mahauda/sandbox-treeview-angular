import { Deserializable } from './deserializable.model';
import { v4 as uuid } from 'uuid';

export class Element implements Deserializable {
    public id: string;
    public pid: string;
    public level: number;
    public position: number;
    public name: string;
    public description: string;
    public expanded: boolean;
    public selected: boolean;
    public childrens: Element[];
    public hasChildren: boolean;

    /**
     * Désérialiser un JSON en objet
     * @param input 
     */
    public deserialize(input: any): this {
        Object.assign(this, input);
        this.childrens = input.childrens.map(element => new Element().deserialize(element));
        return this;
    }

    /**
     * Trier les éléments
     */
    public sort(): void {
        for(var e of this.childrens) {
            e.sort();
        }

        this.childrens.sort((a, b) => a.position - b.position);
    }

    /**
     * Supprimer un élément par son id
     * @param id 
     */
    public remove(id: string): void {
        for(var e of this.childrens) {
            e.remove(id);
        }

        if (this.childrens.find(e => e.id == id)) {
            this.childrens.splice(this.childrens.findIndex(e => e.id == id), 1);
         }
    }

    /**
     * Récupèrer un élément par son id dans la hiérarchie
     * @param id 
     */
    public find(id: string): Element {
        if(this.id == id) {
            return this;
        } else {
            for(var e of this.childrens) {
                var find = e.find(id);
                if(find != null)
                    return find;
            }
            return null;
        }
    }

    /**
     * Cloner un élément
     * @param pid 
     */
    public clone(pid: string): Element {
        var target = new Element();

        target.id = uuid();
        target.pid = pid;
        target.level = this.level;
        target.position = this.position;
        target.name = this.name;
        target.description = this.description;
        target.expanded = this.expanded;
        target.selected = false;
        target.hasChildren = this.hasChildren;
        target.childrens = []

        for(var element of this.childrens) {
            target.childrens.push(element.clone(target.id));
        }

        return target;
    }

    /**
     * Mise à jour d'un élément à partir d'un noeud
     * @param node 
     * @param level 
     * @param position 
     */
    public updateElementFromNode(node: { [key: string]: Object }, level: number, position: number): void {
        this.id = node.id as string;
        this.pid = node.pid as string;
        this.level = level;
        this.position = position;
        this.name = node.name as string;
        this.expanded = node.expanded as boolean || false;
        this.selected = node.selected as boolean || false;
        this.hasChildren = node.hasChildren as boolean || false;
    }

    /**
     * Convertir un élément en un noeud pour l'afficher dans le treeview
     */
    public toNode(): { [key: string]: Object } {
        return {
            id: this.id,
            text: this.name,
            parentID: this.pid,
            child: this.childrens,
            hasChildren: this.hasChildren,
            expanded: this.expanded,
            selected: this.selected
        };
    }
}
