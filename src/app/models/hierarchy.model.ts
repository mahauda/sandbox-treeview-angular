import { Deserializable } from './deserializable.model';
import { Element } from './element.model';

export class Hierarchy implements Deserializable {
    public id: string;
    public name: string;
    public description: string;
    public root: Element;

    /**
     * Désérialiser du JSON en objet
     * @param input 
     */
    public deserialize(input: any): this {
        Object.assign(this, input);
        this.root = new Element().deserialize(input.root);
        return this;
    }

    /**
     * Trier les éléments
     */
    public sort(): void {
        this.root.sort();
    }

    /**
     * Supprime un élément par son id
     * @param id 
     */
    public remove(id: string): void {
        this.root.remove(id);
    }

    /**
     * Récupérer un élément par son id
     * @param id 
     */
    public find(id: string): Element {
        return this.root.find(id);
    }
}
