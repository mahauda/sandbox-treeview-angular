import { Component, OnInit } from '@angular/core';
import { Cours } from './models/cours.model';
import { CoursService } from './services/cours.service';
import { StructureService } from './services/structure.service';
import { TacheService } from './services/tache.service';
import { Tache } from './models/tache.model';
import { Structure } from './models/structure.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public cours : Cours[];
  public structures : Structure[];
  public taches : Tache[];

  constructor(
    private coursService : CoursService,
    private structureService : StructureService,
    private tacheService : TacheService
    ) {}

  ngOnInit(): void {
    this.coursService.getAllCours().subscribe(cours => this.cours = cours);
    console.log("Cours : " + JSON.stringify(this.cours));

    this.structureService.getAllStructure().subscribe(structs => this.structures = structs);
    console.log("Structures : " + JSON.stringify(this.structures));

    this.tacheService.getAllTache().subscribe(taches => this.taches = taches);
    console.log("Taches : " + JSON.stringify(this.taches));
  }

  public onSaveCours(): void {
    for(var c of this.cours) {
      this.coursService.updateCours(c).subscribe(cours => console.log("cours update", cours));
    }
  }

  public onSaveStructures(): void {
    for(var s of this.structures) {
      this.structureService.updateStructure(s).subscribe(struct => console.log("structure update", struct));
    }
  }
}
