import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Cours } from '../models/cours.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Tache } from '../models/tache.model';

const API_URL = 'http://localhost:8080/project-web-jee-bomberman/api/tache';
const HTPP_OPTIONS = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class TacheService {

  constructor(private httpService: HttpClient) { }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  /**
   * Récupère tous les cours par la méthode GET
   */
  public getAllTache(): Observable<Tache[]> {
    return this.httpService
      .get<Tache[]>(API_URL)
      .pipe(
        map(data => data.map(data => new Tache().deserialize(data))),
        catchError(this.handleError<Tache[]>('getAllTache', []))
    );
  }

  /**
   * Mise à jour d'un cours par la méthode PUT
   * @param cours 
   */
  public updateTache(tache: Tache): Observable<Tache> {
    return this.httpService
      .put<Tache>(API_URL, tache)
      .pipe(
        catchError(this.handleError<Tache>('updateTache', tache))
      );
  }
}