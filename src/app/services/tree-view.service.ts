import { Injectable } from '@angular/core';
import { Element } from '../models/element.model';
import { TreeViewComponent } from '../components/tree-view/tree-view.component';

@Injectable({
  providedIn: 'root'
})
export class TreeViewService {

  /**
   * Le tree view sélectionné
   */
  public treeViewSelected: TreeViewComponent;

  /**
   * Les éléments copiés depuis le clique droit
   */
  public copyElements: Element[];

  /**
   * Faut-il supprimer les éléments copiés après collés ?
   */
  public removeElements: boolean;

  constructor() { }

  /**
   * Supprimer les éléments copiés
   */
  public removeCopyElements() : void {
    if(!this.removeElements) return;

    console.log("remove copy elements");
    console.log("id treeview", this.treeViewSelected.id);

    this.copyElements.map(element => {
      this.treeViewSelected.hierarchy.remove(element.id)
      this.treeViewSelected.tree.removeNodes([element.id]);
    });

    this.treeViewSelected.tree.refresh();
  }
}
