import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Cours } from '../models/cours.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Structure } from '../models/structure.model';

const API_URL = 'http://localhost:8080/project-web-jee-bomberman/api/structure';
const HTPP_OPTIONS = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class StructureService {

  constructor(private httpService: HttpClient) { }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  /**
   * Récupère toutes les structures par la méthode GET
   */
  public getAllStructure(): Observable<Structure[]> {
    return this.httpService
      .get<Structure[]>(API_URL)
      .pipe(
        map(data => data.map(data => new Structure().deserialize(data))),
        catchError(this.handleError<Structure[]>('getAllStructure', []))
    );
  }

  /**
   * Mise à jour d'une structure par la méthode PUT
   * @param cours 
   */
  public updateStructure(structure: Structure): Observable<Structure> {
    return this.httpService
      .put<Cours>(API_URL, structure)
      .pipe(
        catchError(this.handleError<Structure>('updateStructure', structure))
      );
  }
}