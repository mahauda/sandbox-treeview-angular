import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { TreeViewModule, ContextMenuModule } from '@syncfusion/ej2-angular-navigations';
import { AppComponent } from './app.component';
import { TreeViewComponent } from './components/tree-view/tree-view.component';
import { CoursService } from './services/cours.service';
import { TreeViewService } from './services/tree-view.service';
import { StructureService } from './services/structure.service';
import { TacheService } from './services/tache.service';

@NgModule({
  declarations: [
    AppComponent,
    TreeViewComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule, 
    TreeViewModule, 
    ContextMenuModule
  ],
  providers: [
    TreeViewService,
    CoursService,
    StructureService,
    TacheService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
